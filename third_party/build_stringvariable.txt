Windows AMD64:
cd StringVariable
if [[ ! -d out/win32_amd64 ]]; then
	mkdir -p out/win32_amd64
fi
cd out/win32_amd64
cmake ../../ -G"Visual Studio 14 2015 Win64" -DMSVC_RUNTIME=static -DCMAKE_INSTALL_PREFIX=.
MSBuild.exe //p:Configuration=Release //p:Platform=x64 INSTALL.vcxproj
